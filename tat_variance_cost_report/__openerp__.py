##############################################################################
#
#    Darmawan Fatriananda
#    -
#    Copyright (c) 2015 <http://www.telenais.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################
{
    "name": "Variance Cost Report ",
    "version": "1.3",
    "author": "Darmawan Fatriananda",
    "category": "Reporting",
    "description": """
        :Stock Move Reporting Module: 
           Variance Cost Module : 
           Stock Movement Report categorize by Begining,In , Out, And Ending
           Will summary by product consumed
        
        Menu : Reporting -> Warehouse -> Variance Cost Report
        
    """,
    "website" : "http://www.telenais.com",
    "license" : "GPL-3",
    "depends": ['purchase','stock','mrp',],
    'update_xml': ["variance_cost_report_view.xml",],
    'installable': True,
    'active': False,
}
