##############################################################################
#
#    Darmawan Fatriananda
#    -
#    Copyright (c) 2015 <http://www.telenais.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from osv import fields, osv


class variance_cost_report(osv.osv_memory):
    
    _name = "variance.cost.report"
    _description="Variance Cost Report"
    _columns = {
    'from_date'  : fields.date('From',required=True),
    'end_date'    : fields.date('To',required=True),
    'product_category_ids': fields.many2many("product.category", string="Product Category"),
    'actual_cost'  : fields.float('Actual Cost',required=True),
    }
    #_defaults = {
    #'actual_cost':65000, 
    #'from_date' :'2015-10-01',
    #'end_date' :'2015-10-30',
    #}

    def get_variance_cost_report(self, cr, uid, ids, context={}):
        value = self.read(cr, uid, ids)[0]
        if value['from_date'] > value['end_date'] :
            raise osv.except_osv(_('Error !'), _('Periode Tanggal Akhir Tidak Boleh Kurang Dari Tanggal Awal  !'))
        datas = {
            'ids': context.get('active_ids',[]),
            'model': 'variance.cost.report',
            'form': value
        }
	
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'variance.cost.xls',
            'report_type': 'webkit',
            'datas': datas,
        }
    
variance_cost_report()
