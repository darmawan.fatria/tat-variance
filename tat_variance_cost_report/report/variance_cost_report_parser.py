import pooler
import time
from datetime import datetime
import tools
import logging
from report import report_sxw
from report_webkit import webkit_report

class variance_cost_report_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context=None):
        super(variance_cost_report_parser, self).__init__(cr, uid, name, context=context)

    def get_variance_cost_report_raw(self,filters,context=None):
        date_from = filters['form']['from_date']
        date_to = filters['form']['end_date']
        actual_cost = filters['form']['actual_cost']
        product_category_ids = filters['form']['product_category_ids']
        

        date_filter = """ and  ( date >= to_date('"""+date_from+ """','YYYY-MM-DD')
                                     and date < to_date('"""+date_to+ """','YYYY-MM-DD') + interval '1 days'  )
                """
        category_filter=None
        is_by_category=False;
        child_of_product_category_ids = []
        if product_category_ids :
            category_filter = ' where categ_id in ( 0 '
            for category_id in product_category_ids :
                category_filter = category_filter + ',%s' % (category_id,)
                category_childs = self.get_child_ids(category_id);
                for child in category_childs :
                    category_filter = category_filter + ',%s' % (child,)
                    child_of_product_category_ids.append(child);
            category_filter = category_filter + ' )'
            #print "Category Filter : ",category_filter
            is_by_category = True 

        for new_child_id in child_of_product_category_ids:
                product_category_ids.append(new_child_id)

        print "product_category_ids  : ",product_category_ids
        query =self._get_main_query_string(date_filter)
        #print "query : ",query
        self.cr.execute(query)
        result = self.cr.fetchall()
        
        total_amount_in_cost = self._get_total_amount_in_cost(query)
        total_amount_in_cost_category= 0
        if is_by_category:
            total_amount_in_cost_category = self._get_total_amount_in_cost_by_category(query,category_filter)

        actual_cost_diff = actual_cost - total_amount_in_cost
        actual_cost_diff_sub = total_amount_in_cost # total amout if without categories, 
        if is_by_category :
            actual_cost_diff_sub = total_amount_in_cost_category

        return self.get_main_data_in_list(result,filters,actual_cost,total_amount_in_cost,total_amount_in_cost_category,is_by_category,actual_cost_diff,actual_cost_diff_sub,product_category_ids,True,True,context=None)

    def get_child_ids(self,category_id):
        product_category_pool = self.pool.get('product.category')
        category_ids=product_category_pool.search(self.cr, self.uid, [('id','child_of',category_id)], context=None)
        #print " child category ids : ",category_ids
        return category_ids;

    def init_variance_cost_report(self,filters,context=None):
        date_from = filters['form']['from_date']
        date_to = filters['form']['end_date']
        
        date_filter = """ and   ending_date < to_date('"""+date_from+ """','YYYY-MM-DD') + interval '1 days' 
                """
        query ="""
                select product_id,ending_date,ending_qty,ending_amount
                        from variance_cost_data vcd
                            where  (  ending_date  = ( 
                                    select max(ending_date) 
                                    from variance_cost_data max_vcd
                                    where max_vcd.product_id = vcd.product_id
                                          """+date_filter+"""
                                    )
                                )
                """
        #print "query : ",query
        self.cr.execute(query)
        data = []
        result = self.cr.fetchall()
        for product_id,ending_date,ending_qty,ending_amount in result:
            new_dict = {}
            new_dict['product_id'] = product_id
            new_dict['ending_date'] = ending_date
            new_dict['ending_qty'] = ending_qty
            new_dict['ending_amount'] = ending_amount

            data.append(new_dict)

        return data

    def init_variance_cost_report_raw(self,filters,context=None):
        date_from = filters['form']['from_date']
        date_to = filters['form']['end_date']
        actual_cost = 0
        product_category_ids = None
        category_filter=None
        is_by_category=False;
        
        date_filter = """ and   date < to_date('"""+date_from+ """','YYYY-MM-DD') + interval '1 days' 
                """
        query =self._get_main_query_string(date_filter)
        #print "query : ",query
        total_amount_in_cost = self._get_total_amount_in_cost(query)
        total_amount_in_cost_category= 0
        actual_cost_diff = 0
        actual_cost_diff_sub = total_amount_in_cost # total amout if without categories, 
        self.cr.execute(query)
        result = self.cr.fetchall()
        return self.get_main_data_in_list(result,filters,actual_cost,total_amount_in_cost,total_amount_in_cost_category,is_by_category,actual_cost_diff,actual_cost_diff_sub,product_category_ids,False,False,context=None)
       

    def _get_total_amount_in_cost(self,sub_query):
        query = """
                select sum( coalesce(qty_in,0) * coalesce(unit_std_cost,0) )  total_amount_in_cost  from 
                (   """+sub_query+"""
                ) var_rep_query
                """
        self.cr.execute(query)
        result = self.cr.fetchall()
        total_amount_in_qty =0 
        for amount_in_qty in result:
            for amt in amount_in_qty:
                if amt :
                    total_amount_in_qty+=amt
            

        return  total_amount_in_qty

    def _get_total_amount_in_cost_by_category(self,sub_query,category_filter):
        query = """
                select sum( coalesce(qty_in,0) * coalesce(unit_std_cost,0)  )  total_amount_in_cost  from 
                (   """+sub_query+"""
                ) var_rep_query 
                """ + category_filter + """
                """
        self.cr.execute(query)
        result = self.cr.fetchall()
        total_amount_in_qty =0 
        for amount_in_qty in result:
            for amt in amount_in_qty:
                if amt :
                    total_amount_in_qty+=amt
            

        return  total_amount_in_qty

    def get_main_data_in_list(self,result,filters,actual_cost,total_amount_in_cost,total_amount_in_cost_category,is_by_category,actual_cost_diff,actual_cost_diff_sub,product_category_ids,current_transaction,is_store,context=None):
        data = []
        data_in_category = []
        data_not_in_category = []
        idx = 1
        if current_transaction: 
            latest_data_transaction = self.init_variance_cost_report(filters);
        

        for product_id,product_name,category_id,qty,unit_std_cost,qty_in,qty_out in result:
            new_dict = {}
            new_dict['no_idx'] = idx
            new_dict['product_id'] = product_id
            new_dict['product_name'] = product_name
            new_dict['qty'] = qty
            new_dict['unit_std_cost'] = unit_std_cost
            new_dict['qty_in'] = qty_in
            new_dict['qty_out'] = qty_out
            new_dict['amount'] = qty * unit_std_cost

            if new_dict['qty']  == 0 and  new_dict['amount']  == 0 and  current_transaction :
                qty, latest_ending_amount = self.get_qty_and_amount_in_latest(product_id,latest_data_transaction)
                new_dict['qty'] = qty
                new_dict['amount'] = latest_ending_amount
                
            variance_cost =0
            qty_avl = qty + qty_in
            qty_ending = qty_avl - qty_out
            
            new_dict['amount_in_cost'] = qty_in * unit_std_cost
            #print "total_amount_in_cost : ",total_amount_in_cost
            #print "actual_cost : ",actual_cost
            if total_amount_in_cost != 0 and actual_cost>0 :
                if actual_cost_diff_sub != 0 :
                    variance_cost = ( new_dict['amount_in_cost'] / actual_cost_diff_sub ) * actual_cost_diff
            
            #if not is_by_category and new_dict['qty_in'] <= 0 :
                #dont set variance cost to 0 if qty_in > 0
             #   variance_cost = 0
            if is_by_category and category_id not in product_category_ids :
                variance_cost = 0
            new_dict['variance_cost'] = variance_cost
            new_dict['actual_cost'] = new_dict['variance_cost'] + new_dict['amount_in_cost']
            new_dict['qty_avl'] = qty_avl
            new_dict['amount_avl'] = new_dict['amount']  + new_dict['actual_cost']
            actual_cost_unit = 0
            if new_dict['qty_avl']  > 0 :
                actual_cost_unit = new_dict['amount_avl'] / new_dict['qty_avl']
            new_dict['actual_cost_unit'] = actual_cost_unit
            new_dict['qty_ending'] = qty_ending
            new_dict['amount_ending'] = actual_cost_unit * qty_ending
            new_dict['product_consumed'] = new_dict['amount_avl'] - new_dict['amount_ending'] 
            
            data.append(new_dict)
            idx+=1

            if is_store:
                ending_date = filters['form']['end_date']
                self.create_or_update_variance_cost(new_dict['product_id'] ,ending_date,new_dict['qty_ending'],new_dict['amount_ending'])
            #sorting by category
            if is_by_category :
                if category_id in product_category_ids :
                    data_in_category.append(new_dict)
                else :
                    data_not_in_category.append(new_dict);

        if is_by_category :
            #print "sorted by category "
            sorted_data = self.merge_list(data_in_category,data_not_in_category)
        else :
            #print "sorted init"
            sorted_data = sorted(data, key=lambda obj: obj['variance_cost'],reverse=True)
            
        return sorted_data

    def _get_main_query_string(self,date_filter):
        query ="""
                select p.id,'['||p.default_code||'] '||pt.name product_name,pt.categ_id
                        ,coalesce(sum(beg.sum_product_qty),0) beg_qty ,  coalesce(sum(pt.standard_price),0) unit_std_cost
                        ,coalesce(sum(s_in.sum_product_qty),0) qty_in
                        ,coalesce(sum(s_out.sum_product_qty),0) qty_out
                from product_product p
                left join product_template pt on pt.id = p.product_tmpl_id
                left join ( select s.product_id,s.location_id,sum(s.product_qty) sum_product_qty
                        from  stock_move s,
                             stock_location loc
                        where 
                        s.location_id = loc.id and
                        state='done' and
                        loc.usage = 'inventory' """+date_filter+"""
                        group by    s.product_id,s.location_id  
                        ) beg on beg.product_id = p.id
                left join ( select s.product_id, sum(s.product_qty) sum_product_qty
                        from  stock_move s
                        left join stock_location orig on s.location_id =orig.id and orig.usage not in( 'inventory') and lower(orig.complete_name) not like  '%"""+'stock'+ """%' 
                        left join stock_location dest on s.location_dest_id = dest.id and  dest.usage  not in( 'inventory')and lower(dest.complete_name) like '%"""+'stock'+ """%' 
                        where 
                        orig.id notnull and 
                        dest.id notnull and
                        state='done' """+date_filter+"""
                        group by s.product_id
                        ) s_in on s_in.product_id = p.id
                left join ( select s.product_id, sum(s.product_qty) sum_product_qty
                        from  stock_move s
                        left join stock_location orig on s.location_id =orig.id and orig.usage not in( 'inventory') and lower(orig.complete_name) like '%"""+'stock'+ """%' 
                        left join stock_location dest on s.location_dest_id = dest.id and  dest.usage not in( 'inventory') and lower(dest.complete_name) not like '%"""+'stock'+ """%' 
                        where 
                        orig.id notnull and 
                        dest.id notnull and
                        state='done' """+date_filter+"""
                        group by s.product_id 
                        ) s_out on s_out.product_id = p.id
                where( p.default_code notnull) 
                group by p.id,p.default_code,pt.name,pt.categ_id
                 
            """
            #where( p.product_id notnull or  s_out.product_id notnull or s_in.product_id notnull) 
        return query

    def get_qty_and_amount_in_latest(self,product_id,latest_data_transaction):

        for arr_data in latest_data_transaction:
            if arr_data['product_id'] == product_id :
                return arr_data['ending_qty'],arr_data['ending_amount']

        return 0,0
    def create_or_update_variance_cost(self,product_id,ending_date,ending_qty,ending_amount):
        vc_data_pool = self.pool.get('variance.cost.data')
        exist_ids =  vc_data_pool.search(self.cr, self.uid, [('ending_date','=',ending_date),('product_id','=',product_id)])
        vc_data_param =  {}
        vc_data_param.update({
                           'product_id':product_id,
                           'ending_date':ending_date,
                           'ending_qty':ending_qty,
                           'ending_amount':ending_amount,
                           })
        if exist_ids :
            #update
            vc_data_pool.write(self.cr,self.uid,exist_ids,vc_data_param)
        else :
            # insert
            vc_data_pool.create(self.cr, self.uid, vc_data_param)

    def merge_list(self,data1,data2):
        sorted_data=sorted(data1, key=lambda obj: obj['variance_cost'],reverse=True);
        #print " 1. ",len(sorted_data)
        for aData in data2:
            sorted_data.append(aData)
        #print " 2. ",len(sorted_data)
        return sorted_data;
    def _multikeysort(self,items, columns):
        from operator import itemgetter
        comparers = [ ((itemgetter(col[1:].strip()), -1) if col.startswith('-') else (itemgetter(col.strip()), 1)) for col in columns]
        def comparer(left, right):
            for fn, mult in comparers:
                result = cmp(fn(left), fn(right))
                if result:
                    return mult * result
            else:
                return 0
        return sorted(items, cmp=comparer)
    
    def date_to_string(self,date):
        datefrom=''
        if date:
            try:
                datefrom = time.strftime('%d/%m/%Y', time.strptime(date,'%Y-%m-%d'))
            except ValueError:
                i=True
                #print "Oops!  That was no valid date format.  Try again..."

        return datefrom
    
