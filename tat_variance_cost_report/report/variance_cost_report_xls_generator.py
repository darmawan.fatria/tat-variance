import time
import xlwt
import cStringIO
from xlwt import Workbook, Formula
from report_engine_xls import report_xls
import variance_cost_report_xls_generator
import variance_cost_report_parser
from variance_cost_report_parser import variance_cost_report_parser

class variance_cost_report_xls_generator(report_xls):

    def generate_xls_report(self, parser, filters, obj, workbook):
            worksheet = workbook.add_sheet(('Variance Cost'))
            worksheet.panes_frozen = True
            worksheet.remove_splits = True
            worksheet.portrait = True # Landscape
            worksheet.fit_wiresult_datah_to_pages = 1
            worksheet.col(1).wiresult_datah = len("ABCDEFGIJKL")*1024

            # Styles (It's used for writing rows / headers)
            row_normal_style=  xlwt.easyxf(num_format_str='#,##0.00;(#,##0.00)')
            info_style = xlwt.easyxf('font: height 200, name Arial, colour_index white, bold on, italic off; align: wrap on, vert centre;pattern: pattern solid, fore_color gray50;', num_format_str='#,##0.00;(#,##0.00)')
            top_style = xlwt.easyxf('font: height 200, name Arial, colour_index white, bold on, italic off; align: wrap on, vert centre, horiz center;pattern: pattern solid, fore_color gray50;', num_format_str='#,##0.00;(#,##0.00)')
            header_style = xlwt.easyxf('font: height 200, name Arial, colour_index white, bold on, italic off; align: wrap on, horiz centre, horiz centre;pattern: pattern solid, fore_color gray50;', num_format_str='#,##0.00;(#,##0.00)')
            sum_style = xlwt.easyxf('font: height 200, name Arial, colour_index white, bold on, italic off; align: wrap on, vert centre;pattern: pattern solid, fore_color gray50;', num_format_str='#,##0.00;(#,##0.00)')
            int_number_style=  xlwt.easyxf(num_format_str='#,##0;(#,##0)')

            # Specifying columns, the order doesn't matter
            # lamda d,f,p: is a function who has filter,data,parser as the parameters it is expected to the value of the column
            cols_specs = [
            # ('header', column_span, column_type, lamda function)

            # Info And Title
            ('Title',  2, 0, 'text', lambda x, d, p: 'Variance Cost Report'),
            ('Period', 2, 0, 'text', lambda x, d, p: p.date_to_string(x['form']['from_date']) +' - '+p.date_to_string(x['form']['end_date'])),

            # Top Headers And Merging
            ('Begining',  2, 100, 'text', lambda x, d, p: 'Begining'),
            ('Unit Std',  1, 100, 'text', lambda x, d, p: 'Unit Std'),
            ('Incoming',  4, 100, 'text', lambda x, d, p: 'Incoming'),
            ('Available',  3, 100, 'text', lambda x, d, p: 'Available'),
            ('Ending',  2, 100, 'text', lambda x, d, p: 'Ending'),
            ('P Consumed',  1, 100, 'text', lambda x, d, p: 'Product'),

            # Column Data
            ('No', 1, 35, 'number', lambda x, d, p: d['no_idx'],xlwt.Row.set_cell_number,int_number_style),
            ('Product', 1, 150, 'text', lambda x, d, p: d['product_name']),
            ('Qty', 1, 45, 'number', lambda x, d, p: d['qty'] ,xlwt.Row.set_cell_number,int_number_style),
            ('Amount', 1, 85, 'number', lambda x, d, p: d['amount']  ), 
            ('Cost', 1, 85, 'number', lambda x, d, p: d['unit_std_cost'] ), 
            ('Qty In', 1, 45, 'number', lambda x, d, p: d['qty_in']  ,xlwt.Row.set_cell_number,int_number_style),
            ('Amount In Cost', 1, 85, 'number', lambda x, d, p: d['amount_in_cost'] ), 
            ('Variance Cost', 1, 85, 'number', lambda x, d, p: d['variance_cost'] ), 
            ('Actual Cost', 1, 85, 'number', lambda x, d, p: d['actual_cost'] ), 
            ('Qty Avl', 1, 45, 'number', lambda x, d, p: d['qty_avl']  ,xlwt.Row.set_cell_number,int_number_style),
            ('Amount Avl', 1, 85, 'number', lambda x, d, p: d['amount_avl']  ), 
            ('Actual Unit Cost', 1, 85, 'number', lambda x, d, p: d['actual_cost_unit']  ),
            ('Qty Out', 1, 45, 'number', lambda x, d, p: d['qty_out']  ,xlwt.Row.set_cell_number,int_number_style),
            ('Qty Out Ending', 1, 45, 'number', lambda x, d, p: d['qty_ending']  ,xlwt.Row.set_cell_number,int_number_style),
            ('Amount Ending', 1, 85, 'number', lambda x, d, p: d['amount_ending']   ),  
            ('Consumed', 1, 85, 'number', lambda x, d, p: d['product_consumed']  ),  
            

            # Misc
            ('single_empty_column', 1, 0, 'text', lambda x, d, p: ''),
            ('double_empty_column', 2, 0, 'text', lambda x, d, p: ''),
            ('triple_empty_column', 3, 0, 'text', lambda x, d, p: ''),
            ('quadruple_empty_column', 3, 0, 'text', lambda x, d, p: ''),
            ('fifth_empty_column', 5, 0, 'text', lambda x, d, p: ''),

            ]

            row_spec_value_data = ['No','Product','Qty','Amount','Cost','Qty In','Amount In Cost','Variance Cost',
            'Actual Cost','Qty Avl', 'Amount Avl','Actual Unit Cost', 'Qty Out','Qty Out Ending','Amount Ending','Consumed', ]

            # Row templates (Order Matters, this joins the columns that are specified in the second parameter)
            title_template = self.xls_row_template(cols_specs, ['single_empty_column','Title'])
            period_template = self.xls_row_template(cols_specs, ['single_empty_column','Period'])
            top_header_template = self.xls_row_template(cols_specs, ['double_empty_column','Begining','Unit Std','Incoming','Available','single_empty_column','Ending','P Consumed'])
            #sums_template = self.xls_row_template(cols_specs, ['single_empty_column','Jumlah','quadruple_empty_column','sum_dpp','sum_ppn','sum_total','double_empty_column'])
            empty_row_template = self.xls_row_template(cols_specs, ['single_empty_column'])

            # Write infos
            # xls_write_row(worksheet, filters, data parser, row_number, template, style)
            row_count =0
            self.xls_write_row(worksheet, filters, None, parser, row_count, title_template, info_style)
            row_count+=1
            self.xls_write_row(worksheet, filters, None, parser, row_count, period_template, info_style)
            row_count+=1
            self.xls_write_row(worksheet, filters, None, parser, row_count, empty_row_template, row_normal_style)
            row_count+=1

            darfat_debug=False

            if darfat_debug:
                
                # ---- Get Variance Report Data
                row_template_data = self.xls_row_template(cols_specs,row_spec_value_data)
                # write Header Table
                self.xls_write_row(worksheet, filters, None, parser, row_count, top_header_template, top_style)
                row_count+=1
                # Write Header Table Of Column
                self.xls_write_row_header(worksheet, row_count, row_template_data, header_style, set_column_size=True)
                row_count+=1

                idx=1
                result = parser.init_variance_cost_report_raw(filters);
                for variance_cost_data in result:
                        # Write Rows
                        variance_cost_data['no_idx'] = idx
                        self.xls_write_row(worksheet, filters, variance_cost_data, parser, row_count, row_template_data, row_normal_style)
                        row_count+=1
                        idx+=1
                row_count+=1

            # ---- Get Variance Report Data
            row_template_data = self.xls_row_template(cols_specs,row_spec_value_data)
            # write Header Table
            self.xls_write_row(worksheet, filters, None, parser, row_count, top_header_template, top_style)
            row_count+=1
            # Write Header Table Of Column
            self.xls_write_row_header(worksheet, row_count, row_template_data, header_style, set_column_size=True)
            row_count+=1

            idx=1
            result = parser.get_variance_cost_report_raw(filters);
            for variance_cost_data in result:
                    # Write Rows
                    variance_cost_data['no_idx'] = idx
                    self.xls_write_row(worksheet, filters, variance_cost_data, parser, row_count, row_template_data, row_normal_style)
                    row_count+=1
                    idx+=1
            
    
    # Override from report_engine_xls.py	
    def create_source_xls(self, cr, uid, ids, filters, report_xml, context=None): 
        if not context: context = {}
	
	# Avoiding context's values change
        context_clone = context.copy()
	
        rml_parser = self.parser(cr, uid, self.name2, context=context_clone)
        objects = self.getObjects(cr, uid, ids, context=context_clone)
        rml_parser.set_context(objects, filters, ids, 'xls')
        io = cStringIO.StringIO()
        workbook = xlwt.Workbook(encoding='utf-8')
        self.generate_xls_report(rml_parser, filters, rml_parser.localcontext['objects'], workbook)
        workbook.save(io)
        io.seek(0)
        return (io.read(), 'xls')

#Start the reporting service
variance_cost_report_xls_generator(
    #name (will be referred from variance_cost_report.py, must add "report." as prefix)
    'report.variance.cost.xls',
    #model
    'variance.cost.report',
    #file
    'addons/tat_variance_cost_report/report/variance_cost_report.xls',
    #parser
    parser=variance_cost_report_parser,
    #header
    header=True
)
