##############################################################################
#
#    Darmawan Fatriananda
#    -
#    Copyright (c) 2015 <http://www.telenais.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from osv import fields, osv

class variance_cost_data(osv.osv):
    
    _name = "variance.cost.data"
    _description = "Variance Cost Data"
    _columns = {
    'product_id': fields.many2one("product.product", string="Product",required=True),
    'ending_date'  : fields.date('From',required=True),
    'ending_qty'  : fields.integer('Qty',required=True),
    'ending_amount'  : fields.float('Actual Cost',required=True),
    }

variance_cost_data()